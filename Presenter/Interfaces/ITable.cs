﻿using static Model.Manager;

namespace Presenter.Interfaces
{
    public interface ITable
    {
        object dataSource { set; }
        void AddItemCallbackFn(CRUD Action, object item);
    }
}
