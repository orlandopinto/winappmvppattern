﻿using DAL.Repositories;
using Model.ModelEntities;
using Presenter.Interfaces;
using System;
using System.Collections.Generic;
using static Model.Manager;

namespace Presenter.Presenters
{
    public class TablePresenter
    {
        readonly ITable Vista;
        Repository Repositorio = new Repository();

        public TablePresenter(ITable View) => Vista = View;

        /// <summary>Asigna set de datos al DataSource del Control</summary>
        public void SetDataSource()
        {
            try
            {
                Vista.dataSource = Repositorio.List();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CRUD Action, TableModel TableView)
        {
            try
            {
                Repositorio.Exist(TableView.FieldValue);
                switch (Action)
                {
                    case CRUD.Create:
                        Repositorio.Create(TableView);
                        break;
                    case CRUD.Update:
                        Repositorio.Update(TableView);
                        break;
                }
                SetDataSource();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(int Id)
        {
            try
            {
                Repositorio.Delete(Id);
                SetDataSource();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<TableModel> Listar()
        {
            return Repositorio.List();
        }
    }
}
