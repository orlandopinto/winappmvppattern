﻿using DAL.Entity;
using Model.ModelEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace DAL.Repositories
{
    public class Repository
    {
        /// <summary>Obtiene set de datos</summary>
        /// <returns>Lista de objetos de tipo List<TableModel></returns>
        public List<TableModel> List()
        {
            List<TableModel> result = new List<TableModel>();
            try
            {
                using (DatabaseEntities db = new DatabaseEntities())
                {
                    db.Database.Connection.Open();
                    result = (from data in db.Table select new TableModel { Id = data.Id, FieldValue = data.FieldValue }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(@"Se produjo un error al consultar datos.");
            }
            return result;
        }

        /// <summary>Crea Nuevo registro</summary>
        /// <param name="TableModel">Datosl</param>
        public void Create(TableModel TableModel)
        {
            try
            {
                using (DatabaseEntities db = new DatabaseEntities())
                {
                    db.Database.Connection.Open();
                    Table data = new Table() { Id = TableModel.Id, FieldValue = TableModel.FieldValue };
                    db.Table.Add(data);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(@"Se produjo un error al crear el registro.");
            }
        }

        /// <summary>Actualiza descripcion del campo.</summary>
        /// <param name="TableModel">Datos.</param>
        public void Update(TableModel TableModel)
        {
            try
            {
                using (DatabaseEntities db = new DatabaseEntities())
                {
                    db.Database.Connection.Open();
                    Table data = db.Table.Where(r => r.Id == TableModel.Id).SingleOrDefault();
                    if (data != null)
                    {
                        data.FieldValue = TableModel.FieldValue;
                        db.Entry(data).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(@"Se produjo un error al actualizar el registro.");
            }
        }

        public void Exist(string Description)
        {
            try
            {
                using (DatabaseEntities db = new DatabaseEntities())
                {
                    db.Database.Connection.Open();
                    var Result = db.Table.Where(r => r.FieldValue.Trim().ToLower() == Description.Trim().ToLower()).FirstOrDefault();
                    if (Result != null) throw new Exception(@"Existe un valor con el mismo nombre, por favor verifique.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>Elimina registro</summary>
        /// <param name="TableModel">Datos</param>
        public void Delete(int Id)
        {
            try
            {
                using (DatabaseEntities db = new DatabaseEntities())
                {
                    db.Database.Connection.Open();
                    var result = db.Table.Where(r => r.Id == Id).SingleOrDefault();
                    if (result != null)
                    {
                        db.Table.Remove(result);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(@"Se produjo un error al eliminar el registro.");
            }
        }
    }
}
