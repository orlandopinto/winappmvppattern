﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;

namespace Model
{
    public static class Manager
    {
        public static bool HashError { get; set; }

        public enum CRUD
        {
            [Description(@"Create")]
            Create = 1,
            [Description(@"Read")]
            Read = 2,
            [Description(@"Update")]
            Update = 3,
            [Description(@"Delete")]
            Delete = 4
        }

        public static string ToEnumDescription(this Enum en)
        {
            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());
            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attrs != null && attrs.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }
            return en.ToString();
        }

        public static string UppercaseFirstLetter(this string value)
        {
            if (value.Length > 0)
            {
                char[] array = value.ToCharArray();
                array[0] = char.ToUpper(array[0]);
                return new string(array);
            }
            return value;
        }

        public static Icon ToIcon(this Bitmap value)
        {
            return Icon.FromHandle(value.GetHicon());
        }
    }
}
