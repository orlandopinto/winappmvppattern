﻿using Model;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using View.Core;
using View.Views.TableView;
using WeifenLuo.WinFormsUI.Docking;

namespace View
{
    public partial class frmMain : Form
    {
        PropertyGrid pGrid = new PropertyGrid();
        DockPanel dockPanel = new DockPanel();

        public frmMain()
        {
            InitializeComponent();
            InitDockPanel();
            PropertiesWindows(new PropertiesAppSettings());
        }

        private void ribbonButton2_Click(object sender, EventArgs e)
        {
            AddUserControlToDockContent(ResourceNames.Mantenimiento, Color.White, new ListTableView(dockPanel), ResourceIcons.Folder_007.ToIcon(), autoScroll: false);
        }

        private void ribbonButton3_Click(object sender, EventArgs e)
        {
            AddUserControlToDockContent(ResourceNames.OtherForm, Color.White, new OtherForm(), ResourceIcons.Apple.ToIcon());
        }

        private void roobExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        #region ..:: [  METHODS ] ::..

        public void PropertiesWindows(object PropertyBox)
        {
            pGrid.SelectedObject = PropertyBox;
            pGrid.PropertyValueChanged += delegate (object s, PropertyValueChangedEventArgs args)
            {
                string mensaje = @"Cambio el valor para la propiedad: ";
                switch (args.ChangedItem.PropertyDescriptor.Name)
                {
                    case @"GreetingText":
                        MessageBox.Show(mensaje + @"Greeting Text a: " + args.ChangedItem.Value);
                        break;

                    case @"MaxRepeatRate":
                        MessageBox.Show(mensaje + @"MaxRepeatRate a: " + args.ChangedItem.Value);
                        break;

                    case @"SaveOnClose":
                        MessageBox.Show(mensaje + @"SaveOnClose a: " + args.ChangedItem.Value);
                        break;
                }
            };
        }

        public bool ExistContent(string ContentName)
        {
            return dockPanel.Contents.Where(w => w.DockHandler.Form.Name == ContentName).Any();
        }

        public void InitDockPanel()
        {
            try
            {
                dockPanel.Dock = DockStyle.Fill;
                dockPanel.BackColor = Color.White;
                dockPanel.BackgroundImage = ResourceImages.shopping_cart;
                dockPanel.BackgroundImageLayout = ImageLayout.Center;
                dockPanel.ShowDocumentIcon = true;
                dockPanel.Name = @"dockPanelMain";
                Controls.Add(dockPanel);
                dockPanel.BringToFront();
                AddDockUserControlsToContent(ResourceNames.CuadroHerramientas, DockState.DockLeftAutoHide, Color.White, ResourceIcons.Folder_088.ToIcon(), new Views.Tools.UC_Tools(), DockStyle.Fill);
                AddDockPropertiesContent(ResourceNames.Propiedades, DockState.DockRightAutoHide, Color.White, ResourceIcons.Folder_088.ToIcon());
            }
            catch (Exception ex)
            {
                //Guardar error el Log
            }
        }

        private void AddDockPropertiesContent(string ContentName, DockState dockState, Color color, Icon icon, bool autoScroll = true)
        {
            try
            {
                if (!ExistContent(ContentName))
                {
                    DockContent content = GetDockContentForm(ContentName, dockState, color);
                    content.AutoScroll = autoScroll;
                    content.Icon = icon;
                    content.Controls.Add(pGrid);
                    pGrid.Dock = DockStyle.Fill;
                    pGrid.SelectedObject = dockPanel;
                    content.Show(dockPanel);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveDockContent(string DockContentName)
        {
            if (ExistContent(DockContentName))
                dockPanel.Contents.Where(w => w.DockHandler.Form.Name == DockContentName).FirstOrDefault().DockHandler.Dispose();
        }

        public void AddDockUserControlsToContent(string ContentName, DockState dockState, Color color, Icon icon, UserControl userControl, DockStyle UserControlDockStyle, bool autoScroll = true)
        {
            try
            {
                if (!ExistContent(ContentName))
                {
                    DockContent contentTools = GetDockContentForm(ContentName, dockState, color);
                    contentTools.AutoScroll = autoScroll;
                    contentTools.Icon = icon;
                    userControl.Dock = UserControlDockStyle;
                    contentTools.Controls.Add(userControl);
                    contentTools.Show(dockPanel);
                }
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
        }

        public void AddUserControlToDockContent(string ContentName, Color color, Control control, Icon BitMapIcon, bool autoScroll = true)
        {
            try
            {
                if (!ExistContent(ContentName))
                {
                    DockContent DockContentTabs = GetDockContentForm(ContentName, DockState.Document, color);
                    DockContentTabs.AutoScroll = autoScroll;
                    DockContentTabs.Icon = BitMapIcon;
                    DockContentTabs.Dock = DockStyle.Fill;
                    DockContentTabs.Controls.Add(control);
                    DockContentTabs.Show(dockPanel);
                    control.Width = DockContentTabs.Width;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DockContent GetDockContentForm(string name, DockState showHint, Color backColour)
        {
            DockContent content1 = new DockContent();
            content1.Name = name;
            content1.TabText = name;
            content1.Text = name;
            content1.ShowHint = showHint;
            content1.BackColor = backColour;
            return content1;
        }

        public void ActiveWindow(string DockContentName)
        {
            if (ExistContent(DockContentName))
                dockPanel.Contents.Where(w => w.DockHandler.Form.Name == DockContentName).FirstOrDefault().DockHandler.Activate();
        }

        #endregion
    }
}
