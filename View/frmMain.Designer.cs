﻿namespace View
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.ribbonButton1 = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel4 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel3 = new System.Windows.Forms.RibbonPanel();
            this.rtGuest = new System.Windows.Forms.RibbonTab();
            this.ribbonButton5 = new System.Windows.Forms.RibbonButton();
            this.ribbonButton4 = new System.Windows.Forms.RibbonButton();
            this.rpReservaciones = new System.Windows.Forms.RibbonPanel();
            this.ribbonButton3 = new System.Windows.Forms.RibbonButton();
            this.ribbonButton2 = new System.Windows.Forms.RibbonButton();
            this.rpWizard = new System.Windows.Forms.RibbonPanel();
            this.rtFrontDesk = new System.Windows.Forms.RibbonTab();
            this.ribbonOrbRecentItem2 = new System.Windows.Forms.RibbonOrbRecentItem();
            this.ribbonOrbRecentItem1 = new System.Windows.Forms.RibbonOrbRecentItem();
            this.roobExit = new System.Windows.Forms.RibbonOrbOptionButton();
            this.ribbonOrbMenuItem4 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonSeparator1 = new System.Windows.Forms.RibbonSeparator();
            this.ribbonOrbMenuItem1 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.romiInformation = new System.Windows.Forms.RibbonOrbMenuItem();
            this.StatusBar = new System.Windows.Forms.StatusStrip();
            this.RibbonBar = new System.Windows.Forms.Ribbon();
            this.SuspendLayout();
            // 
            // ribbonButton1
            // 
            this.ribbonButton1.AltKey = null;
            this.ribbonButton1.CheckedGroup = null;
            this.ribbonButton1.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.ribbonButton1.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.ribbonButton1.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.Image")));
            this.ribbonButton1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.SmallImage")));
            this.ribbonButton1.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.ribbonButton1.Tag = null;
            this.ribbonButton1.Text = null;
            this.ribbonButton1.ToolTip = null;
            this.ribbonButton1.ToolTipTitle = null;
            this.ribbonButton1.Value = null;
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.Tag = null;
            this.ribbonPanel4.Text = "ribbonPanel4";
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.Tag = null;
            this.ribbonPanel3.Text = "ribbonPanel3";
            // 
            // rtGuest
            // 
            this.rtGuest.Panels.Add(this.ribbonPanel3);
            this.rtGuest.Panels.Add(this.ribbonPanel4);
            this.rtGuest.Tag = null;
            this.rtGuest.Text = "Guest/Contact";
            this.rtGuest.ToolTip = null;
            this.rtGuest.ToolTipIcon = System.Windows.Forms.ToolTipIcon.None;
            this.rtGuest.ToolTipImage = null;
            this.rtGuest.ToolTipTitle = null;
            this.rtGuest.Value = null;
            // 
            // ribbonButton5
            // 
            this.ribbonButton5.AltKey = null;
            this.ribbonButton5.CheckedGroup = null;
            this.ribbonButton5.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.ribbonButton5.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.ribbonButton5.Image = global::View.ResourceIcons.Office_Lamp;
            this.ribbonButton5.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton5.SmallImage")));
            this.ribbonButton5.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.ribbonButton5.Tag = null;
            this.ribbonButton5.Text = null;
            this.ribbonButton5.ToolTip = null;
            this.ribbonButton5.ToolTipTitle = null;
            this.ribbonButton5.Value = null;
            // 
            // ribbonButton4
            // 
            this.ribbonButton4.AltKey = null;
            this.ribbonButton4.CheckedGroup = null;
            this.ribbonButton4.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.ribbonButton4.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.ribbonButton4.Image = global::View.ResourceIcons.office_chart_bar;
            this.ribbonButton4.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton4.SmallImage")));
            this.ribbonButton4.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.ribbonButton4.Tag = null;
            this.ribbonButton4.Text = null;
            this.ribbonButton4.ToolTip = null;
            this.ribbonButton4.ToolTipTitle = null;
            this.ribbonButton4.Value = null;
            // 
            // rpReservaciones
            // 
            this.rpReservaciones.Items.Add(this.ribbonButton4);
            this.rpReservaciones.Items.Add(this.ribbonButton5);
            this.rpReservaciones.Tag = null;
            this.rpReservaciones.Text = "Reservaciones";
            // 
            // ribbonButton3
            // 
            this.ribbonButton3.AltKey = null;
            this.ribbonButton3.CheckedGroup = null;
            this.ribbonButton3.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.ribbonButton3.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.ribbonButton3.Image = global::View.ResourceIcons.office_calendar;
            this.ribbonButton3.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton3.SmallImage")));
            this.ribbonButton3.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.ribbonButton3.Tag = null;
            this.ribbonButton3.Text = null;
            this.ribbonButton3.ToolTip = null;
            this.ribbonButton3.ToolTipTitle = null;
            this.ribbonButton3.Value = null;
            this.ribbonButton3.Click += new System.EventHandler(this.ribbonButton3_Click);
            // 
            // ribbonButton2
            // 
            this.ribbonButton2.AltKey = null;
            this.ribbonButton2.CheckedGroup = null;
            this.ribbonButton2.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.ribbonButton2.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.ribbonButton2.Image = global::View.ResourceIcons.Apple;
            this.ribbonButton2.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton2.SmallImage")));
            this.ribbonButton2.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.ribbonButton2.Tag = null;
            this.ribbonButton2.Text = null;
            this.ribbonButton2.ToolTip = null;
            this.ribbonButton2.ToolTipTitle = null;
            this.ribbonButton2.Value = null;
            this.ribbonButton2.Click += new System.EventHandler(this.ribbonButton2_Click);
            // 
            // rpWizard
            // 
            this.rpWizard.Items.Add(this.ribbonButton2);
            this.rpWizard.Items.Add(this.ribbonButton3);
            this.rpWizard.Tag = null;
            this.rpWizard.Text = "Wizard";
            // 
            // rtFrontDesk
            // 
            this.rtFrontDesk.Panels.Add(this.rpWizard);
            this.rtFrontDesk.Panels.Add(this.rpReservaciones);
            this.rtFrontDesk.Tag = null;
            this.rtFrontDesk.Text = "FrontDesk";
            this.rtFrontDesk.ToolTip = null;
            this.rtFrontDesk.ToolTipIcon = System.Windows.Forms.ToolTipIcon.None;
            this.rtFrontDesk.ToolTipImage = null;
            this.rtFrontDesk.ToolTipTitle = null;
            this.rtFrontDesk.Value = null;
            // 
            // ribbonOrbRecentItem2
            // 
            this.ribbonOrbRecentItem2.AltKey = null;
            this.ribbonOrbRecentItem2.CheckedGroup = null;
            this.ribbonOrbRecentItem2.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.ribbonOrbRecentItem2.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.ribbonOrbRecentItem2.Image = ((System.Drawing.Image)(resources.GetObject("ribbonOrbRecentItem2.Image")));
            this.ribbonOrbRecentItem2.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonOrbRecentItem2.SmallImage")));
            this.ribbonOrbRecentItem2.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.ribbonOrbRecentItem2.Tag = null;
            this.ribbonOrbRecentItem2.Text = "Recent Document 2";
            this.ribbonOrbRecentItem2.ToolTip = null;
            this.ribbonOrbRecentItem2.ToolTipTitle = null;
            this.ribbonOrbRecentItem2.Value = null;
            // 
            // ribbonOrbRecentItem1
            // 
            this.ribbonOrbRecentItem1.AltKey = null;
            this.ribbonOrbRecentItem1.CheckedGroup = null;
            this.ribbonOrbRecentItem1.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.ribbonOrbRecentItem1.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.ribbonOrbRecentItem1.Image = ((System.Drawing.Image)(resources.GetObject("ribbonOrbRecentItem1.Image")));
            this.ribbonOrbRecentItem1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonOrbRecentItem1.SmallImage")));
            this.ribbonOrbRecentItem1.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.ribbonOrbRecentItem1.Tag = null;
            this.ribbonOrbRecentItem1.Text = "Recent Document 1";
            this.ribbonOrbRecentItem1.ToolTip = null;
            this.ribbonOrbRecentItem1.ToolTipTitle = null;
            this.ribbonOrbRecentItem1.Value = null;
            // 
            // roobExit
            // 
            this.roobExit.AltKey = null;
            this.roobExit.CheckedGroup = null;
            this.roobExit.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.roobExit.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.roobExit.Image = null;
            this.roobExit.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.roobExit.Tag = null;
            this.roobExit.Text = "Exit WinAppMVPPattern";
            this.roobExit.ToolTip = null;
            this.roobExit.ToolTipTitle = null;
            this.roobExit.Value = null;
            // 
            // ribbonOrbMenuItem4
            // 
            this.ribbonOrbMenuItem4.AltKey = null;
            this.ribbonOrbMenuItem4.CheckedGroup = null;
            this.ribbonOrbMenuItem4.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem4.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.ribbonOrbMenuItem4.Image = global::View.ResourceIcons.H_D_03;
            this.ribbonOrbMenuItem4.SmallImage = global::View.ResourceIcons.H_D_03;
            this.ribbonOrbMenuItem4.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.ribbonOrbMenuItem4.Tag = null;
            this.ribbonOrbMenuItem4.Text = "New";
            this.ribbonOrbMenuItem4.ToolTip = null;
            this.ribbonOrbMenuItem4.ToolTipTitle = null;
            this.ribbonOrbMenuItem4.Value = null;
            // 
            // ribbonSeparator1
            // 
            this.ribbonSeparator1.AltKey = null;
            this.ribbonSeparator1.CheckedGroup = null;
            this.ribbonSeparator1.Image = null;
            this.ribbonSeparator1.Tag = null;
            this.ribbonSeparator1.Text = null;
            this.ribbonSeparator1.ToolTip = null;
            this.ribbonSeparator1.ToolTipTitle = null;
            this.ribbonSeparator1.Value = null;
            // 
            // ribbonOrbMenuItem1
            // 
            this.ribbonOrbMenuItem1.AltKey = null;
            this.ribbonOrbMenuItem1.CheckedGroup = null;
            this.ribbonOrbMenuItem1.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem1.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.ribbonOrbMenuItem1.Image = global::View.ResourceIcons.Folder_007;
            this.ribbonOrbMenuItem1.SmallImage = global::View.ResourceIcons.Folder_007;
            this.ribbonOrbMenuItem1.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.ribbonOrbMenuItem1.Tag = null;
            this.ribbonOrbMenuItem1.Text = "Save";
            this.ribbonOrbMenuItem1.ToolTip = null;
            this.ribbonOrbMenuItem1.ToolTipTitle = null;
            this.ribbonOrbMenuItem1.Value = null;
            // 
            // romiInformation
            // 
            this.romiInformation.AltKey = null;
            this.romiInformation.CheckedGroup = null;
            this.romiInformation.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.romiInformation.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.romiInformation.Image = global::View.ResourceIcons.Folder_088;
            this.romiInformation.SmallImage = global::View.ResourceIcons.Folder_088;
            this.romiInformation.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.romiInformation.Tag = null;
            this.romiInformation.Text = "Reload";
            this.romiInformation.ToolTip = null;
            this.romiInformation.ToolTipTitle = null;
            this.romiInformation.Value = null;
            // 
            // StatusBar
            // 
            this.StatusBar.Location = new System.Drawing.Point(0, 552);
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.Size = new System.Drawing.Size(959, 22);
            this.StatusBar.TabIndex = 4;
            this.StatusBar.Text = "statusStrip1";
            // 
            // RibbonBar
            // 
            this.RibbonBar.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.RibbonBar.Location = new System.Drawing.Point(0, 0);
            this.RibbonBar.Name = "RibbonBar";
            // 
            // 
            // 
            this.RibbonBar.OrbDropDown.BorderRoundness = 8;
            this.RibbonBar.OrbDropDown.Location = new System.Drawing.Point(0, 0);
            this.RibbonBar.OrbDropDown.MenuItems.Add(this.romiInformation);
            this.RibbonBar.OrbDropDown.MenuItems.Add(this.ribbonOrbMenuItem1);
            this.RibbonBar.OrbDropDown.MenuItems.Add(this.ribbonSeparator1);
            this.RibbonBar.OrbDropDown.MenuItems.Add(this.ribbonOrbMenuItem4);
            this.RibbonBar.OrbDropDown.Name = "";
            this.RibbonBar.OrbDropDown.OptionItems.Add(this.roobExit);
            this.RibbonBar.OrbDropDown.RecentItems.Add(this.ribbonOrbRecentItem1);
            this.RibbonBar.OrbDropDown.RecentItems.Add(this.ribbonOrbRecentItem2);
            this.RibbonBar.OrbDropDown.RecentItemsCaption = null;
            this.RibbonBar.OrbDropDown.Size = new System.Drawing.Size(527, 207);
            this.RibbonBar.OrbDropDown.TabIndex = 0;
            this.RibbonBar.OrbImage = null;
            this.RibbonBar.OrbStyle = System.Windows.Forms.RibbonOrbStyle.Office_2010;
            this.RibbonBar.OrbText = "Inicio";
            // 
            // 
            // 
            this.RibbonBar.QuickAcessToolbar.AltKey = null;
            this.RibbonBar.QuickAcessToolbar.Image = null;
            this.RibbonBar.QuickAcessToolbar.Tag = null;
            this.RibbonBar.QuickAcessToolbar.Text = null;
            this.RibbonBar.QuickAcessToolbar.ToolTip = null;
            this.RibbonBar.QuickAcessToolbar.ToolTipTitle = null;
            this.RibbonBar.QuickAcessToolbar.Value = null;
            this.RibbonBar.Size = new System.Drawing.Size(959, 138);
            this.RibbonBar.TabIndex = 3;
            this.RibbonBar.Tabs.Add(this.rtFrontDesk);
            this.RibbonBar.Tabs.Add(this.rtGuest);
            this.RibbonBar.TabsMargin = new System.Windows.Forms.Padding(12, 26, 20, 0);
            this.RibbonBar.TabSpacing = 6;
            this.RibbonBar.Text = "WinAppMVPPattern";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(959, 574);
            this.Controls.Add(this.StatusBar);
            this.Controls.Add(this.RibbonBar);
            this.IsMdiContainer = true;
            this.Name = "frmMain";
            this.Text = "frmMain";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RibbonButton ribbonButton1;
        private System.Windows.Forms.RibbonPanel ribbonPanel4;
        private System.Windows.Forms.RibbonPanel ribbonPanel3;
        private System.Windows.Forms.RibbonTab rtGuest;
        private System.Windows.Forms.RibbonButton ribbonButton5;
        private System.Windows.Forms.RibbonButton ribbonButton4;
        private System.Windows.Forms.RibbonPanel rpReservaciones;
        private System.Windows.Forms.RibbonButton ribbonButton3;
        private System.Windows.Forms.RibbonButton ribbonButton2;
        private System.Windows.Forms.RibbonPanel rpWizard;
        private System.Windows.Forms.RibbonTab rtFrontDesk;
        private System.Windows.Forms.RibbonOrbRecentItem ribbonOrbRecentItem2;
        private System.Windows.Forms.RibbonOrbRecentItem ribbonOrbRecentItem1;
        private System.Windows.Forms.RibbonOrbOptionButton roobExit;
        private System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem4;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator1;
        private System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem1;
        private System.Windows.Forms.RibbonOrbMenuItem romiInformation;
        private System.Windows.Forms.StatusStrip StatusBar;
        private System.Windows.Forms.Ribbon RibbonBar;
    }
}