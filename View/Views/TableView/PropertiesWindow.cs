﻿using System.ComponentModel;
using System.Drawing;

namespace View.Views.TableView
{
    [DefaultPropertyAttribute("SaveOnClose")]
    public class PropertiesWindow
    {
        int m_DisplayInt = 50;                      // some initialization

        [ReadOnly(true)]                            // but just read only
        [Description("sample hint1")]               // sample hint1
        [Category("Category1")]                     // Category that I want
        [DisplayName("Int for Displaying")]         // I want to say more, than just DisplayInt
        public int DisplayInt
        {
            get { return m_DisplayInt; }
            set { m_DisplayInt = value; }
        }

        string m_DisplayString = "James Bond";

        [ReadOnly(false)]                           // this property is for editing
        [Description("Example Displaying hint 2")]  // sample hint2
        [Category("Datos Personales")]              // Category that I want
        [DisplayName("Nombre y Apellido")]          // and more than Display String
        public string DisplayString
        {
            get { return m_DisplayString; }
            set { m_DisplayString = value; }
        }

        int m_Edad = 40;
        [ReadOnly(false)]                            // this property is for editing
        [Description("Example Displaying hint 2")]   // sample hint2
        [Category("Datos Personales")]               // Category that I want
        [DisplayName("Edad en Años")]                // and more than Display String
        public int Edad
        {
            get { return m_Edad; }
            set { m_Edad = value; }
        }

        bool m_DisplayBool;
        [Category("Category2")]                    // Category that I want
        [Description("To be or not to be")]        // yet one hint
        [DisplayName("To drink or not to drink")]  // that is a question
        public bool DisplayBool
        {
            get { return m_DisplayBool; }
            set { m_DisplayBool = value; }
        }

        Color m_DisplayColors;
        public Color DisplayColors
        {
            get { return m_DisplayColors; }
            set { m_DisplayColors = value; }
        }

    }
}
