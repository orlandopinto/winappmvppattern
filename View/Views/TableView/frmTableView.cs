﻿using MetroFramework.Forms;
using Model.ModelEntities;
using System;
using System.Windows.Forms;
using static Model.Manager;

namespace View.Views.TableView
{
    public partial class frmTableView : MetroForm
    {
        public delegate void AddItemDelegate(CRUD Action, object item);
        public AddItemDelegate AddItemCallback;
        TableModel model;
        CRUD Action;

        public frmTableView(CRUD Action, TableModel model)
        {
            this.Action = Action;
            InitializeComponent();
            this.model = model;
            txtDescription.Text = string.Empty;

            switch (Action)
            {
                case CRUD.Create:
                    lblID.Visible = false;
                    lblDataID.Visible = false;
                    this.Text = @"Agregar Nuevo Regsitro";
                    break;

                case CRUD.Update:
                    this.Text = @"Actualizar registro actual";
                    lblDataID.Text = model.Id.ToString();
                    txtDescription.Text = model.FieldValue;
                    break;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                AddItemCallback(Action, new TableModel() { Id = model.Id, FieldValue = txtDescription.Text.UppercaseFirstLetter() });
                if (!HashError) this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Guardar datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
