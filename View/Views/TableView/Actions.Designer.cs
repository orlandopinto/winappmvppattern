﻿namespace View.Views.TableView
{
    partial class Actions
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.LnkActualizar = new MetroFramework.Controls.MetroLink();
            this.LnkEliminar = new MetroFramework.Controls.MetroLink();
            this.LnkModificar = new MetroFramework.Controls.MetroLink();
            this.LnkAgregar = new MetroFramework.Controls.MetroLink();
            this.SuspendLayout();
            // 
            // LnkActualizar
            // 
            this.LnkActualizar.Image = global::View.ResourceIcons.Refresh;
            this.LnkActualizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LnkActualizar.Location = new System.Drawing.Point(12, 24);
            this.LnkActualizar.Name = "LnkActualizar";
            this.LnkActualizar.Size = new System.Drawing.Size(129, 23);
            this.LnkActualizar.TabIndex = 12;
            this.LnkActualizar.Text = "Refrescar Lista";
            this.LnkActualizar.UseSelectable = true;
            this.LnkActualizar.Click += new System.EventHandler(this.LnkActualizar_Click);
            // 
            // LnkEliminar
            // 
            this.LnkEliminar.Image = global::View.ResourceIcons.delete;
            this.LnkEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LnkEliminar.Location = new System.Drawing.Point(12, 112);
            this.LnkEliminar.Name = "LnkEliminar";
            this.LnkEliminar.Size = new System.Drawing.Size(93, 23);
            this.LnkEliminar.TabIndex = 11;
            this.LnkEliminar.Text = "Eliminar";
            this.LnkEliminar.UseSelectable = true;
            this.LnkEliminar.Click += new System.EventHandler(this.LnkEliminar_Click);
            // 
            // LnkModificar
            // 
            this.LnkModificar.Image = global::View.ResourceIcons.edit;
            this.LnkModificar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LnkModificar.Location = new System.Drawing.Point(12, 83);
            this.LnkModificar.Name = "LnkModificar";
            this.LnkModificar.Size = new System.Drawing.Size(101, 23);
            this.LnkModificar.TabIndex = 10;
            this.LnkModificar.Text = "Modificar";
            this.LnkModificar.UseSelectable = true;
            this.LnkModificar.Click += new System.EventHandler(this.LnkModificar_Click);
            // 
            // LnkAgregar
            // 
            this.LnkAgregar.Image = global::View.ResourceIcons.Plus;
            this.LnkAgregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LnkAgregar.Location = new System.Drawing.Point(12, 53);
            this.LnkAgregar.Name = "LnkAgregar";
            this.LnkAgregar.Size = new System.Drawing.Size(93, 23);
            this.LnkAgregar.TabIndex = 9;
            this.LnkAgregar.Text = "Agregar";
            this.LnkAgregar.UseSelectable = true;
            this.LnkAgregar.Click += new System.EventHandler(this.LnkAgregar_Click);
            // 
            // Actions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.LnkActualizar);
            this.Controls.Add(this.LnkEliminar);
            this.Controls.Add(this.LnkModificar);
            this.Controls.Add(this.LnkAgregar);
            this.Name = "Actions";
            this.Size = new System.Drawing.Size(317, 534);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroLink LnkActualizar;
        private MetroFramework.Controls.MetroLink LnkEliminar;
        private MetroFramework.Controls.MetroLink LnkModificar;
        private MetroFramework.Controls.MetroLink LnkAgregar;
    }
}
