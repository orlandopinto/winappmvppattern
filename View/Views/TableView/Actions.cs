﻿using Model.ModelEntities;
using Presenter.Interfaces;
using Presenter.Presenters;
using System;
using System.Windows.Forms;
using static Model.Manager;
using static View.Views.TableView.frmTableView;

namespace View.Views.TableView
{
    public partial class Actions : UserControl
    {
        static TablePresenter Presenter;
        ListView LstViewHotelView;

        public Actions(ListView LstViewHotelView, ITable Vista)
        {
            InitializeComponent();
            Presenter = new TablePresenter(Vista);
            this.LstViewHotelView = LstViewHotelView;
        }

        private void LnkAgregar_Click(object sender, EventArgs e)
        {
            frmTableView dlg = new frmTableView(CRUD.Create, new TableModel());
            dlg.AddItemCallback = new AddItemDelegate(AddItemCallbackFn);
            dlg.ShowDialog();
        }

        public void AddItemCallbackFn(CRUD Action, object item)
        {
            try
            {
                Presenter.Save(Action, (TableModel)item);
                HashError = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"Guardar Vista Hotel", MessageBoxButtons.OK, MessageBoxIcon.Error);
                HashError = true;
            }
        }

        private void LnkModificar_Click(object sender, EventArgs e)
        {
            try
            {
                if (LstViewHotelView.SelectedItems.Count > 0)
                {
                    frmTableView dlg = new frmTableView(CRUD.Update, new TableModel() { Id = Convert.ToInt32(LstViewHotelView.SelectedItems[0].Text), FieldValue = LstViewHotelView.SelectedItems[0].SubItems[1].Text });
                    dlg.AddItemCallback = new AddItemDelegate(AddItemCallbackFn);
                    dlg.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"Guardar Vista Hotel", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LnkActualizar_Click(object sender, EventArgs e)
        {
            Presenter.SetDataSource();
        }

        private void LnkEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (LstViewHotelView.SelectedItems.Count > 0)
                {
                    var confirmResult = MessageBox.Show("¿Esta seguro que quiere eliminar el registro?", "Confirmar eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (confirmResult == DialogResult.Yes) Presenter.Delete(Convert.ToInt32(LstViewHotelView.SelectedItems[0].Text));
                }
                else
                    MessageBox.Show(@"Seleccione elemento a eliminar.", "Eliminar registro", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
