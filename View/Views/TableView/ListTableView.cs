﻿using Model.ModelEntities;
using Presenter.Interfaces;
using Presenter.Presenters;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using View.Core;
using WeifenLuo.WinFormsUI.Docking;
using static Model.Manager;
using static View.Views.TableView.frmTableView;

namespace View.Views.TableView
{
    public partial class ListTableView : UserControl, ITable
    {
        TablePresenter Presenter;
        readonly ITable HotelView;
        DockPanel dockPanel;
        frmMain MainFormulary;

        #region ..:: [ PROPERTIES ] ::..

        public object dataSource
        {
            set
            {
                LstViewHotelView.Items.Clear();
                foreach (var data in (List<TableModel>)value)
                {
                    ListViewItem item = new ListViewItem(data.Id.ToString());
                    item.SubItems.Add(data.FieldValue);
                    LstViewHotelView.Items.Add(item);
                }
            }
        }

        #endregion

        public ListTableView(DockPanel dockPanel)
        {
            try
            {
                InitializeComponent();
                this.dockPanel = dockPanel;
                Presenter = new TablePresenter(this);
                HotelView = this;
                LoadListView();
                if (Application.OpenForms[@"frmMain"] != null) MainFormulary = Application.OpenForms[@"frmMain"] as frmMain;
                MainFormulary.AddDockUserControlsToContent(ResourceNames.Acciones, DockState.DockRightAutoHide, Color.White, ResourceIcons.Folder_088.ToIcon(), new Actions(LstViewHotelView, HotelView), DockStyle.Fill);
                MainFormulary.PropertiesWindows(new PropertiesWindow());
                this.Disposed += ListHotelView_Disposed;
            }
            catch (Exception ex)
            {
                //Register error
            }
        }

        #region ..:: [ METHODS ] ::..

        public void AddItemCallbackFn(CRUD Action, object item)
        {
            try
            {
                Presenter.Save(Action, (TableModel)item);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Guardar datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LoadListView()
        {
            try
            {
                Presenter.SetDataSource();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Guardar datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        private void tsmiVentana_Click(object sender, EventArgs e)
        {
            try
            {
                MainFormulary.AddDockUserControlsToContent(ResourceNames.Acciones, DockState.DockRightAutoHide, Color.White, ResourceIcons.Folder_088.ToIcon(), new Actions(LstViewHotelView, HotelView), DockStyle.Fill);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            MainFormulary.ActiveWindow(ResourceNames.Acciones);
        }

        private void LnkAgregar_Click(object sender, EventArgs e)
        {
            frmTableView dlg = new frmTableView(CRUD.Create, new TableModel());
            dlg.AddItemCallback = new AddItemDelegate(AddItemCallbackFn);
            dlg.ShowDialog();
        }

        private void ListHotelView_Disposed(object sender, EventArgs e)
        {
            try
            {
                MainFormulary.RemoveDockContent(ResourceNames.Acciones);
                MainFormulary.PropertiesWindows(new PropertiesAppSettings());
            }
            catch (ArgumentNullException ane)
            {
                string error = ane.Message;
            }
            catch (ArgumentException arg)
            {
                string error = arg.Message;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
        }

        private void tsbClose_Click(object sender, EventArgs e)
        {
            Form CurrentFrm = this.FindForm();
            CurrentFrm.Close();
            CurrentFrm.Dispose();
            MainFormulary.RemoveDockContent(ResourceNames.Acciones);
            MainFormulary.PropertiesWindows(new PropertiesAppSettings());
        }

        private void LnkActualizar_Click(object sender, EventArgs e)
        {
            LoadListView();
        }

        private void LnkEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (LstViewHotelView.SelectedItems.Count > 0)
                {
                    var confirmResult = MessageBox.Show("¿Esta seguro que quiere eliminar el registro?", "Confirmar eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (confirmResult == DialogResult.Yes) Presenter.Delete(Convert.ToInt32(LstViewHotelView.SelectedItems[0].Text));
                }
                else
                    MessageBox.Show(@"Seleccione elemento a eliminar.", "Eliminar registro", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LnkModificar_Click(object sender, EventArgs e)
        {
            try
            {
                if (LstViewHotelView.SelectedItems.Count > 0)
                {
                    frmTableView dlg = new frmTableView(CRUD.Update, new TableModel() { Id = Convert.ToInt32(LstViewHotelView.SelectedItems[0].Text), FieldValue = LstViewHotelView.SelectedItems[0].SubItems[1].Text });
                    dlg.AddItemCallback = new AddItemDelegate(AddItemCallbackFn);
                    dlg.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void tsmiAgregar_Click(object sender, EventArgs e)
        {
            frmTableView dlg = new frmTableView(CRUD.Create, new TableModel());
            dlg.AddItemCallback = new AddItemDelegate(AddItemCallbackFn);
            dlg.ShowDialog();
        }

        private void tsmiRefrescar_Click(object sender, EventArgs e)
        {

        }

        private void VModificar_Click(object sender, EventArgs e)
        {

        }

        private void tsmiEliminar_Click(object sender, EventArgs e)
        {

        }
    }
}
