﻿namespace View.Views.TableView
{
    partial class ListTableView
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.tsmiEliminar = new System.Windows.Forms.ToolStripMenuItem();
            this.VModificar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAgregar = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.LstViewHotelView = new System.Windows.Forms.ListView();
            this.ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Descripcion = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolStripHeader = new System.Windows.Forms.ToolStrip();
            this.tsmiRefrescar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiVentana = new System.Windows.Forms.ToolStripMenuItem();
            this.tsdbActions = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsbProperties = new System.Windows.Forms.ToolStripButton();
            this.toolStripMainMenu = new System.Windows.Forms.ToolStrip();
            this.tsbClose = new System.Windows.Forms.ToolStripButton();
            this.LblTitle = new MetroFramework.Controls.MetroLabel();
            this.PnlContainer = new MetroFramework.Controls.MetroPanel();
            this.PnlMainContainer = new MetroFramework.Controls.MetroPanel();
            this.PnlBlueTop = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.toolStripMainMenu.SuspendLayout();
            this.PnlContainer.SuspendLayout();
            this.PnlMainContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // tsmiEliminar
            // 
            this.tsmiEliminar.Image = global::View.ResourceIcons.delete;
            this.tsmiEliminar.Name = "tsmiEliminar";
            this.tsmiEliminar.Size = new System.Drawing.Size(191, 22);
            this.tsmiEliminar.Text = "Eliminar";
            this.tsmiEliminar.Click += new System.EventHandler(this.tsmiEliminar_Click);
            // 
            // VModificar
            // 
            this.VModificar.Image = global::View.ResourceIcons.edit;
            this.VModificar.Name = "VModificar";
            this.VModificar.Size = new System.Drawing.Size(191, 22);
            this.VModificar.Text = "Modificar";
            this.VModificar.Click += new System.EventHandler(this.VModificar_Click);
            // 
            // tsmiAgregar
            // 
            this.tsmiAgregar.Image = global::View.ResourceIcons.Plus;
            this.tsmiAgregar.Name = "tsmiAgregar";
            this.tsmiAgregar.Size = new System.Drawing.Size(191, 22);
            this.tsmiAgregar.Text = "Agregar";
            this.tsmiAgregar.Click += new System.EventHandler(this.tsmiAgregar_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::View.ResourceImages.add;
            this.pictureBox1.Location = new System.Drawing.Point(9, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 32);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 53;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.LstViewHotelView);
            this.panel2.Controls.Add(this.toolStripHeader);
            this.panel2.Location = new System.Drawing.Point(8, 41);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1313, 438);
            this.panel2.TabIndex = 52;
            // 
            // LstViewHotelView
            // 
            this.LstViewHotelView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LstViewHotelView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ID,
            this.Descripcion});
            this.LstViewHotelView.FullRowSelect = true;
            this.LstViewHotelView.HideSelection = false;
            this.LstViewHotelView.Location = new System.Drawing.Point(3, 26);
            this.LstViewHotelView.MultiSelect = false;
            this.LstViewHotelView.Name = "LstViewHotelView";
            this.LstViewHotelView.Size = new System.Drawing.Size(533, 407);
            this.LstViewHotelView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.LstViewHotelView.TabIndex = 59;
            this.LstViewHotelView.UseCompatibleStateImageBehavior = false;
            this.LstViewHotelView.View = System.Windows.Forms.View.Details;
            // 
            // ID
            // 
            this.ID.Text = "ID";
            this.ID.Width = 111;
            // 
            // Descripcion
            // 
            this.Descripcion.Text = "Descripcion";
            this.Descripcion.Width = 421;
            // 
            // toolStripHeader
            // 
            this.toolStripHeader.Location = new System.Drawing.Point(0, 0);
            this.toolStripHeader.Name = "toolStripHeader";
            this.toolStripHeader.Size = new System.Drawing.Size(1311, 25);
            this.toolStripHeader.TabIndex = 57;
            this.toolStripHeader.Text = "toolStrip2";
            // 
            // tsmiRefrescar
            // 
            this.tsmiRefrescar.Image = global::View.ResourceIcons.Refresh;
            this.tsmiRefrescar.Name = "tsmiRefrescar";
            this.tsmiRefrescar.Size = new System.Drawing.Size(191, 22);
            this.tsmiRefrescar.Text = "Refrescar";
            this.tsmiRefrescar.Click += new System.EventHandler(this.tsmiRefrescar_Click);
            // 
            // tsmiVentana
            // 
            this.tsmiVentana.Image = global::View.ResourceIcons.new_window;
            this.tsmiVentana.Name = "tsmiVentana";
            this.tsmiVentana.Size = new System.Drawing.Size(191, 22);
            this.tsmiVentana.Text = "Abrir/Mostrar Ventana";
            this.tsmiVentana.Click += new System.EventHandler(this.tsmiVentana_Click);
            // 
            // tsdbActions
            // 
            this.tsdbActions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiVentana,
            this.tsmiRefrescar,
            this.tsmiAgregar,
            this.VModificar,
            this.tsmiEliminar});
            this.tsdbActions.Image = global::View.ResourceIcons.Oxygen_Icons_org_Oxygen_Actions_dialog_ok_apply;
            this.tsdbActions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsdbActions.Name = "tsdbActions";
            this.tsdbActions.Size = new System.Drawing.Size(84, 22);
            this.tsdbActions.Text = "Acciones";
            // 
            // tsbProperties
            // 
            this.tsbProperties.Image = global::View.ResourceIcons.Properties;
            this.tsbProperties.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbProperties.Name = "tsbProperties";
            this.tsbProperties.Size = new System.Drawing.Size(92, 22);
            this.tsbProperties.Text = "Propiedades";
            // 
            // toolStripMainMenu
            // 
            this.toolStripMainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbProperties,
            this.tsdbActions,
            this.tsbClose});
            this.toolStripMainMenu.Location = new System.Drawing.Point(0, 0);
            this.toolStripMainMenu.Name = "toolStripMainMenu";
            this.toolStripMainMenu.Size = new System.Drawing.Size(1321, 25);
            this.toolStripMainMenu.TabIndex = 49;
            this.toolStripMainMenu.Text = "toolStrip3";
            // 
            // tsbClose
            // 
            this.tsbClose.Image = global::View.ResourceIcons.CloseWindow;
            this.tsbClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbClose.Name = "tsbClose";
            this.tsbClose.Size = new System.Drawing.Size(59, 22);
            this.tsbClose.Text = "Cerrar";
            this.tsbClose.ToolTipText = "Cerrar";
            this.tsbClose.Click += new System.EventHandler(this.tsbClose_Click);
            // 
            // LblTitle
            // 
            this.LblTitle.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.LblTitle.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.LblTitle.Location = new System.Drawing.Point(44, 6);
            this.LblTitle.Name = "LblTitle";
            this.LblTitle.Size = new System.Drawing.Size(359, 29);
            this.LblTitle.TabIndex = 51;
            this.LblTitle.Text = "LISTA VALORES";
            // 
            // PnlContainer
            // 
            this.PnlContainer.BackColor = System.Drawing.Color.Transparent;
            this.PnlContainer.Controls.Add(this.pictureBox1);
            this.PnlContainer.Controls.Add(this.panel2);
            this.PnlContainer.Controls.Add(this.LblTitle);
            this.PnlContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this.PnlContainer.HorizontalScrollbarBarColor = true;
            this.PnlContainer.HorizontalScrollbarHighlightOnWheel = false;
            this.PnlContainer.HorizontalScrollbarSize = 10;
            this.PnlContainer.Location = new System.Drawing.Point(0, 25);
            this.PnlContainer.Name = "PnlContainer";
            this.PnlContainer.Size = new System.Drawing.Size(1321, 482);
            this.PnlContainer.TabIndex = 56;
            this.PnlContainer.VerticalScrollbarBarColor = true;
            this.PnlContainer.VerticalScrollbarHighlightOnWheel = false;
            this.PnlContainer.VerticalScrollbarSize = 10;
            // 
            // PnlMainContainer
            // 
            this.PnlMainContainer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PnlMainContainer.BackColor = System.Drawing.Color.Transparent;
            this.PnlMainContainer.Controls.Add(this.PnlContainer);
            this.PnlMainContainer.Controls.Add(this.toolStripMainMenu);
            this.PnlMainContainer.HorizontalScrollbarBarColor = true;
            this.PnlMainContainer.HorizontalScrollbarHighlightOnWheel = false;
            this.PnlMainContainer.HorizontalScrollbarSize = 10;
            this.PnlMainContainer.Location = new System.Drawing.Point(5, 6);
            this.PnlMainContainer.Name = "PnlMainContainer";
            this.PnlMainContainer.Size = new System.Drawing.Size(1321, 510);
            this.PnlMainContainer.TabIndex = 45;
            this.PnlMainContainer.VerticalScrollbarBarColor = true;
            this.PnlMainContainer.VerticalScrollbarHighlightOnWheel = false;
            this.PnlMainContainer.VerticalScrollbarSize = 10;
            // 
            // PnlBlueTop
            // 
            this.PnlBlueTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PnlBlueTop.BackColor = System.Drawing.Color.SteelBlue;
            this.PnlBlueTop.Location = new System.Drawing.Point(0, 0);
            this.PnlBlueTop.Name = "PnlBlueTop";
            this.PnlBlueTop.Size = new System.Drawing.Size(1336, 4);
            this.PnlBlueTop.TabIndex = 44;
            // 
            // ListTableView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.PnlMainContainer);
            this.Controls.Add(this.PnlBlueTop);
            this.Name = "ListTableView";
            this.Size = new System.Drawing.Size(1334, 530);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.toolStripMainMenu.ResumeLayout(false);
            this.toolStripMainMenu.PerformLayout();
            this.PnlContainer.ResumeLayout(false);
            this.PnlMainContainer.ResumeLayout(false);
            this.PnlMainContainer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ToolStripMenuItem tsmiEliminar;
        private System.Windows.Forms.ToolStripMenuItem VModificar;
        private System.Windows.Forms.ToolStripMenuItem tsmiAgregar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListView LstViewHotelView;
        private System.Windows.Forms.ColumnHeader ID;
        private System.Windows.Forms.ColumnHeader Descripcion;
        private System.Windows.Forms.ToolStrip toolStripHeader;
        private System.Windows.Forms.ToolStripMenuItem tsmiRefrescar;
        private System.Windows.Forms.ToolStripMenuItem tsmiVentana;
        private System.Windows.Forms.ToolStripDropDownButton tsdbActions;
        private System.Windows.Forms.ToolStripButton tsbProperties;
        private System.Windows.Forms.ToolStrip toolStripMainMenu;
        private System.Windows.Forms.ToolStripButton tsbClose;
        private MetroFramework.Controls.MetroLabel LblTitle;
        private MetroFramework.Controls.MetroPanel PnlContainer;
        private MetroFramework.Controls.MetroPanel PnlMainContainer;
        private System.Windows.Forms.Panel PnlBlueTop;
    }
}
