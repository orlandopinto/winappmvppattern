﻿namespace View.Views.Tools
{
    partial class UC_Tools
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.naviBarTools = new Guifreaks.NavigationBar.NaviBar(this.components);
            this.naviBand2 = new Guifreaks.NavigationBar.NaviBand(this.components);
            this.naviBand4 = new Guifreaks.NavigationBar.NaviBand(this.components);
            this.naviBand1 = new Guifreaks.NavigationBar.NaviBand(this.components);
            this.naviBand3 = new Guifreaks.NavigationBar.NaviBand(this.components);
            this.naviGroup2 = new Guifreaks.NavigationBar.NaviGroup(this.components);
            this.naviGroup1 = new Guifreaks.NavigationBar.NaviGroup(this.components);
            this.button5 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.naviBarTools)).BeginInit();
            this.naviBarTools.SuspendLayout();
            this.naviBand2.SuspendLayout();
            this.naviBand4.SuspendLayout();
            this.naviBand1.SuspendLayout();
            this.naviBand3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.naviGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.naviGroup1)).BeginInit();
            this.SuspendLayout();
            // 
            // naviBarTools
            // 
            this.naviBarTools.ActiveBand = this.naviBand2;
            this.naviBarTools.Controls.Add(this.naviBand2);
            this.naviBarTools.Controls.Add(this.naviBand4);
            this.naviBarTools.Controls.Add(this.naviBand1);
            this.naviBarTools.Controls.Add(this.naviBand3);
            this.naviBarTools.Dock = System.Windows.Forms.DockStyle.Fill;
            this.naviBarTools.Location = new System.Drawing.Point(0, 130);
            this.naviBarTools.Name = "naviBarTools";
            this.naviBarTools.ShowCollapseButton = false;
            this.naviBarTools.ShowMinimizeButton = false;
            this.naviBarTools.ShowMoreOptionsButton = false;
            this.naviBarTools.Size = new System.Drawing.Size(288, 376);
            this.naviBarTools.TabIndex = 30;
            this.naviBarTools.Text = "naviBar1";
            this.naviBarTools.VisibleLargeButtons = 2;
            // 
            // naviBand2
            // 
            // 
            // naviBand2.ClientArea
            // 
            this.naviBand2.ClientArea.Location = new System.Drawing.Point(0, 0);
            this.naviBand2.ClientArea.Name = "ClientArea";
            this.naviBand2.ClientArea.Size = new System.Drawing.Size(286, 245);
            this.naviBand2.ClientArea.TabIndex = 0;
            this.naviBand2.Location = new System.Drawing.Point(1, 27);
            this.naviBand2.Name = "naviBand2";
            this.naviBand2.Size = new System.Drawing.Size(286, 245);
            this.naviBand2.TabIndex = 0;
            this.naviBand2.Text = "Colors";
            // 
            // naviBand4
            // 
            // 
            // naviBand4.ClientArea
            // 
            this.naviBand4.ClientArea.Location = new System.Drawing.Point(0, 0);
            this.naviBand4.ClientArea.Name = "ClientArea";
            this.naviBand4.ClientArea.Size = new System.Drawing.Size(286, 245);
            this.naviBand4.ClientArea.TabIndex = 0;
            this.naviBand4.Location = new System.Drawing.Point(1, 27);
            this.naviBand4.Name = "naviBand4";
            this.naviBand4.Size = new System.Drawing.Size(286, 245);
            this.naviBand4.TabIndex = 2;
            this.naviBand4.Text = "Tasks";
            // 
            // naviBand1
            // 
            // 
            // naviBand1.ClientArea
            // 
            this.naviBand1.ClientArea.Location = new System.Drawing.Point(0, 0);
            this.naviBand1.ClientArea.Name = "ClientArea";
            this.naviBand1.ClientArea.Size = new System.Drawing.Size(286, 245);
            this.naviBand1.ClientArea.TabIndex = 0;
            this.naviBand1.Location = new System.Drawing.Point(1, 27);
            this.naviBand1.Name = "naviBand1";
            this.naviBand1.Size = new System.Drawing.Size(286, 245);
            this.naviBand1.TabIndex = 4;
            this.naviBand1.Text = "Wizard";
            // 
            // naviBand3
            // 
            // 
            // naviBand3.ClientArea
            // 
            this.naviBand3.ClientArea.Location = new System.Drawing.Point(0, 0);
            this.naviBand3.ClientArea.Name = "ClientArea";
            this.naviBand3.ClientArea.Size = new System.Drawing.Size(286, 245);
            this.naviBand3.ClientArea.TabIndex = 0;
            this.naviBand3.Location = new System.Drawing.Point(1, 27);
            this.naviBand3.Name = "naviBand3";
            this.naviBand3.Size = new System.Drawing.Size(286, 245);
            this.naviBand3.TabIndex = 1;
            this.naviBand3.Text = "Schedule";
            // 
            // naviGroup2
            // 
            this.naviGroup2.Caption = null;
            this.naviGroup2.Dock = System.Windows.Forms.DockStyle.Top;
            this.naviGroup2.Expanded = false;
            this.naviGroup2.ExpandedHeight = 146;
            this.naviGroup2.HeaderContextMenuStrip = null;
            this.naviGroup2.Location = new System.Drawing.Point(0, 110);
            this.naviGroup2.Name = "naviGroup2";
            this.naviGroup2.Padding = new System.Windows.Forms.Padding(1, 22, 1, 1);
            this.naviGroup2.Size = new System.Drawing.Size(288, 20);
            this.naviGroup2.TabIndex = 29;
            this.naviGroup2.Text = "naviGroup2";
            // 
            // naviGroup1
            // 
            this.naviGroup1.Caption = null;
            this.naviGroup1.Dock = System.Windows.Forms.DockStyle.Top;
            this.naviGroup1.ExpandedHeight = 110;
            this.naviGroup1.HeaderContextMenuStrip = null;
            this.naviGroup1.Location = new System.Drawing.Point(0, 0);
            this.naviGroup1.Name = "naviGroup1";
            this.naviGroup1.Padding = new System.Windows.Forms.Padding(1, 22, 1, 1);
            this.naviGroup1.Size = new System.Drawing.Size(288, 110);
            this.naviGroup1.TabIndex = 28;
            this.naviGroup1.Text = "naviGroup1";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(61, 120);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 31;
            this.button5.Text = "button5";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // UC_Tools
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.naviBarTools);
            this.Controls.Add(this.naviGroup2);
            this.Controls.Add(this.naviGroup1);
            this.Controls.Add(this.button5);
            this.Name = "UC_Tools";
            this.Size = new System.Drawing.Size(288, 506);
            ((System.ComponentModel.ISupportInitialize)(this.naviBarTools)).EndInit();
            this.naviBarTools.ResumeLayout(false);
            this.naviBand2.ResumeLayout(false);
            this.naviBand4.ResumeLayout(false);
            this.naviBand1.ResumeLayout(false);
            this.naviBand3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.naviGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.naviGroup1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Guifreaks.NavigationBar.NaviBar naviBarTools;
        private Guifreaks.NavigationBar.NaviBand naviBand2;
        private Guifreaks.NavigationBar.NaviBand naviBand4;
        private Guifreaks.NavigationBar.NaviBand naviBand1;
        private Guifreaks.NavigationBar.NaviBand naviBand3;
        private Guifreaks.NavigationBar.NaviGroup naviGroup2;
        private Guifreaks.NavigationBar.NaviGroup naviGroup1;
        private System.Windows.Forms.Button button5;
    }
}
